| Base 10 | Base 2 | Base 5 | Base 8 | Base 16 | Base 20 |
|---------|--------|--------|--------|---------|---------|
| 0       | 0      | 0      | 00     | $0      | 0       |
| 1       | 1      | 1      | 01     | $1      | 1       |
| 2       | 10     | 2      | 02     | $2      | 2       |
| 3       | 11     | 3      | 03     | $3      | 3       |
| 4       | 100    | 4      | 04     | $4      | 4       |
| 5       | 101    | 10     | 05     | $5      | 5       |
| 6       | 110    | 11     | 06     | $6      | 6       |
| 7       | 111    | 12     | 07     | $7      | 7       |
| 8       | 1000   | 13     | 08     | $8      | 8       |
| 9       | 1001   | 14     | 09     | $9      | 9       |
| 10      | 1010   | 20     | 010    | $A      | A       |
| 11      | 1011   | 21     | 011    | $B      | B       |
| 12      | 1100   | 22     | 012    | $C      | C       |
| 13      | 1101   | 23     | 013    | $D      | D       |
| 14      | 1110   | 24     | 014    | $E      | E       |
| 15      | 1111   | 30     | 015    | $F      | F       |
| 16      | 10000  | 31     | 016    | $10     | G       |
| 17      | 10001  | 32     | 017    | $11     | H       |
| 18      | 10010  | 33     | 018    | $12     | I       |
| 19      | 10011  | 34     | 019    | $13     | J       |
| 20      | 10100  | 40     | 020    | $14     | 10      |
| 21      | 10101  | 41     | 021    | $15     | 11      |
| 22      | 10110  | 42     | 022    | $16     | 12      |
| 23      | 10111  | 43     | 023    | $17     | 13      |
| 24      | 11000  | 44     | 024    | $18     | 14      |
| 25      | 11001  | 100    | 025    | $19     | 15      |
| 26      | 11010  | 101    | 026    | $1A     | 16      |
| 27      | 11011  | 102    | 027    | $1B     | 17      |
| 28      | 11100  | 103    | 028    | $1C     | 18      |
| 29      | 11101  | 104    | 029    | $1D     | 19      |
| 30      | 11110  | 110    | 030    | $1E     | 1A      |
| 31      | 11111  | 111    | 031    | $1F     | 1B      |
| 32      | 100000 | 112    | 032    | $20     | 1C      |
| 33      | 100001 | 113    | 033    | $21     | 1D      |
| 34      | 100010 | 114    | 034    | $22     | 1E      |
| 35      | 100011 | 120    | 035    | $23     | 1F      |
| 36      | 100100 | 121    | 036    | $24     | 1G      |
| 37      | 100101 | 122    | 037    | $25     | 1H      |
| 38      | 100110 | 123    | 038    | $26     | 1I      |
| 39      | 100111 | 124    | 039    | $27     | 1J      |
| 40      | 101000 | 130    | 040    | $28     | 20      |
| 41      | 101001 | 131    | 041    | $29     | 21      |
| 42      | 101010 | 132    | 042    | $2A     | 22      |
| 43      | 101011 | 133    | 043    | $2B     | 23      |
| 44      | 101100 | 134    | 044    | $2C     | 24      |
| 45      | 101101 | 140    | 045    | $2D     | 25      |
| 46      | 101110 | 141    | 046    | $2E     | 26      |
| 47      | 101111 | 142    | 047    | $2F     | 27      |
| 48      | 110000 | 143    | 048    | $30     | 28      |
| 49      | 110001 | 144    | 049    | $31     | 29      |
| 50      | 110010 | 200    | 050    | $32     | 2A      |